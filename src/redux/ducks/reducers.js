import { combineReducers } from 'redux';

import modalReducer from './modalRducer';

import unidades_saude_maker_Reducer from './unidades_saude_maker';

export default combineReducers({
  unidades_saude_maker_Reducer,
  modalReducer,
});
