import { createActions, createReducer } from 'reduxsauce';
import database from '../../services/datas';

/**
 * Action types & creators
 */
export const { Types, Creators } = createActions({
  updateIsVisible: ['id'],
});
const INITIAL_STATE = { datas: database };

const updateVisible = (state = INITIAL_STATE, action) => {
  const { datas } = state;
  const newState = datas.map(unidadeSaude =>
    unidadeSaude.id === action.id
      ? { ...unidadeSaude, is_visible: !unidadeSaude.is_visible }
      : unidadeSaude,
  );
  return newState;
};

export default createReducer(INITIAL_STATE, {
  [Types.UPDATE_IS_VISIBLE]: updateVisible,
});
