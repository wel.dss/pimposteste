import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  updateModal: ['id'],
});

const updateModalData = (state = INITIAL_STATE, action) => {
  const { id, isVisible } = state;
  if (id !== action.id && isVisible) {
    return { ...state, id: action.id };
  }
  const newState = { id: action.id, isVisible: !isVisible };

  return newState;
};

const INITIAL_STATE = { id: 1, isVisible: false };

export default createReducer(INITIAL_STATE, {
  [Types.UPDATE_MODAL]: updateModalData,
});
