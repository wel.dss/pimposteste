import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'react-native';

import Agendamento from './screens/agendamento';
import Details from './screens/detailsUnidadeSaude';

const HootStack = createStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <HootStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#6b257a',
          },
          headerTintColor: '#fff',
        }}>
        <HootStack.Screen
          name="agendamento"
          component={Agendamento}
          options={{
            title: 'Serviços de Saúde',
          }}
        />
        <HootStack.Screen
          name="details"
          options={{
            title: 'Unidade de Saúde',
          }}
          component={Details}
        />
      </HootStack.Navigator>
      <StatusBar barStyle="light-content" backgroundColor="#6b257a" />
    </NavigationContainer>
  );
}
export default Routes;
