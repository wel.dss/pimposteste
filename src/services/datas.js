const data = [
  {
    id: 1,
    name: 'Unidade Municipal de Saúde Baía do Sol',
    endereco: 'Av Beira Mar s/no, próximo ao mercado municipal de Mosqueiro',
    bairro: 'Baia do Sol',
    tipo_servico: 'Urgência e Emergência, 24 horas',
    fone: ['3773-1166'],
    Detalhes_Unidade: 'Sem informação',
    coordinate: {
      latitude: -1.06594,
      longitude: -48.332045,
    },
  },
  {
    id: 2,
    name: 'Unidade Municipal de Saúde Cotijuba',
    endereco: 'Rua Manoel Barata s/no, em frente à Igreja São Francisco',
    bairro: 'Ilha de Cotijuba',
    tipo_servico: 'Urgência e Emergência, 24 horas',
    fone: ['3617-1105', '36171100'],
    Detalhes_Unidade:
      'A unidade conta com uma sala de parto totalmente equipada, com sonar, aspirador, focos, cilindro de oxigênio, berço aquecedor, PA de coluna, mesa ginecológica, mesa métrica para bebê, balança para bebê, balança para adulto e outros. Urgência e Emergência: todos os dias.',
    coordinate: {
      latitude: -1.263725,
      longitude: -48.555823,
    },
  },
  {
    id: 3,
    name: 'Unidade Municipal de Saúde Icoaraci',
    endereco:
      'Rua Manoel Barata, no 840, entre Itaboraí e São Roque, próximo ao colégio Madre Celeste',
    bairro: 'Centro',
    tipo_servico: 'Urgência e Emergência, 24 horas',
    fone: ['3227-0270', '3227-1550'],
    Detalhes_Unidade: 'Sem informação',
    coordinate: {
      latitude: -1.297977,
      longitude: -48.486966,
    },
  },
];

export default data;
