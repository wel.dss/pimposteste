import React from 'react';
import { Marker } from 'react-native-maps';
import { useDispatch } from 'react-redux';

const MarkerComponent = ({ data }) => {
  //console.log('navi', props);
  const dispatch = useDispatch();

  const handlerOnpress = idx => {
    dispatch({
      type: 'UPDATE_MODAL',
      id: idx,
    });
    console.log('click', idx);
  };

  return (
    <Marker
      onPress={() => handlerOnpress(data.id)}
      coordinate={data.coordinate}
      pinColor={true ? '#1cb3b4' : '#69b32d'}
      key={`${data.id} - ${data.is_visible ? ' active ' : ' inactive '}`}
    />
  );
};

export default MarkerComponent;
