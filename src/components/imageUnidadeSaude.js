import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

// import { Container } from './styles';

const ImageRender = source => {
  return (
    <View style={styles.imageContainer}>
      <Image
        style={styles.imageStyle}
        source={{
          uri: 'https://maps.gstatic.com/tactile/pane/default_geocode-1x.png',
        }}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  imageStyle: {
    width: '100%',
    height: 170,
    alignSelf: 'center',
  },
});
export default ImageRender;
