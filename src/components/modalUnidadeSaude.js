import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';

const ModalUnidadeSaude = ({ id, navigation }) => {
  const unidadeSaude = useSelector(state => {
    const { datas } = state.unidades_saude_maker_Reducer;
    const [dataUnidade] = datas.filter(data => data.id === id);
    console.log(dataUnidade);
    return dataUnidade;
  });
  const handleOnpreModal = () => {
    console.log('onPress modal click');
    navigation.navigate('details', { id });
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => handleOnpreModal()}>
      <View style={styles.modalView}>
        <Image
          style={styles.imageStyle}
          source={{
            uri: 'https://maps.gstatic.com/tactile/pane/default_geocode-1x.png',
          }}
        />
        <View style={styles.viewText}>
          <Text style={styles.modalTextTitle}>{unidadeSaude.name}</Text>
          <Text style={styles.modalText}>{unidadeSaude.endereco}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 5,
    left: 10,
    right: 10,
    width: '95%',
    maxHeight: 150,
  },
  modalView: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 3,
    elevation: 5,
    paddingVertical: 15,
    paddingHorizontal: 5,
  },
  viewText: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: 10,
  },
  modalTextTitle: {
    flex: 1,
    fontSize: 13,
    fontWeight: 'bold',
  },
  modalText: {
    flex: 1,
    fontSize: 10,
  },
  imageStyle: {
    width: 70,
    height: 70,
    alignSelf: 'center',
  },
});

export default ModalUnidadeSaude;
