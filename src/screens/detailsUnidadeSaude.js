import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import ImageRender from '../components/imageUnidadeSaude';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFontA from 'react-native-vector-icons/FontAwesome';

const DetailsUnidadeSaude = props => {
  const { id } = props.route.params;
  const unidadeSaude = useSelector(state => {
    const { datas } = state.unidades_saude_maker_Reducer;
    return datas.filter(data => data.id === id);
  });
  const { name, tipo_servico, endereco, fone } = unidadeSaude[0];
  return (
    <View style={styles.container}>
      <ImageRender />
      <View style={styles.containerTexts}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.tipo_servico}>{tipo_servico}</Text>

        <View style={styles.containerFlexDirectionRow}>
          <IconFontA
            style={styles.iconEndereco}
            name="location-arrow"
            color={'#7a7474'}
            size={32}
          />
          <Text style={styles.endereco}>{endereco}</Text>
        </View>

        <View style={styles.containerFlexDirectionRow}>
          <Icon name="phone" color={'#7a7474'} size={32} />
          <View style={styles.containerIconPhoneText}>
            {fone.map(item => (
              <Text key={item} style={styles.textphone}>
                {item}
              </Text>
            ))}
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  containerTexts: {
    marginHorizontal: 10,
    marginTop: 20,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#303030',
  },
  tipo_servico: {
    fontSize: 16,
    color: '#303030',
  },
  endereco: {
    color: '#303030',
    flex: 1,
  },
  iconEndereco: {
    marginRight: 20,
  },
  containerFlexDirectionRow: {
    flexDirection: 'row',
    marginTop: 10,
    marginRight: 10,
    alignItems: 'center',
  },
  containerIconPhoneText: {
    marginHorizontal: 10,
  },
  textphone: {
    marginVertical: 2,
    color: '#303030',
  },
});

export default DetailsUnidadeSaude;
