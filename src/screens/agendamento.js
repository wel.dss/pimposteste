import React, { useState } from 'react';
import { View, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MapView from 'react-native-maps';
import MarkerComponent from '../components/markerComponent';

import ModalUnidadeSaude from '../components/modalUnidadeSaude';
import { useSelector } from 'react-redux';

const Agendamento = ({ navigation }) => {
  const [position, setPosition] = useState({
    latitude: -1.263725,
    longitude: -48.555823,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  const datasMakers = useSelector(state => {
    const { datas } = state.unidades_saude_maker_Reducer;
    return datas;
  });
  const modalOpen = useSelector(state => {
    const { id, isVisible } = state.modalReducer;
    return { id, isVisible };
  });
  //render makers
  const renderMakers = makers =>
    makers.map(maker => <MarkerComponent key={maker.id} data={maker} />);

  const renderModal = (openModal, navi) => {
    if (!openModal.isVisible) {
      return null;
    }
    return <ModalUnidadeSaude id={openModal.id} navigation={navi} />;
  };

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        region={position}
        onPress={e =>
          setPosition({
            ...position,
            latitude: e.nativeEvent.coordinate.latitude,
            longitude: e.nativeEvent.coordinate.longitude,
          })
        }>
        {renderMakers(datasMakers)}
      </MapView>
      <TextInput
        style={styles.containerTextinput}
        placeholder={'pesquise aqui.'}
        placeholderTextColor="#78849e"
      />
      <TouchableOpacity style={styles.locationButton} onPress={() => ({})}>
        <Icon name="my-location" color={'rgb(120, 132, 158)'} size={22} />
      </TouchableOpacity>
      {renderModal(modalOpen, navigation)}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerTextinput: {
    marginTop: 5,
    marginHorizontal: 10,
    borderRadius: 10,
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  locationButton: {
    backgroundColor: 'rgb(253, 253, 253)',
    borderRadius: 15,
    width: 75,
    height: 40,
    right: 10,
    bottom: 150,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'red',
    elevation: 6,
    position: 'absolute',
  },
});

export default Agendamento;
